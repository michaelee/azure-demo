# README #

### Microsoft Azure Demo ###

The code in this repository is based on the sample To-Do List application provided by Microsoft on creation of a Mobile Service. It was written during the recording of the screencast that can be viewed at http://youtu.be/Y1FfakYLbE0

### Steps followed in the screencast ###

1.  Create account at http://azure.microsoft.com
2.  Access Azure portal
    
3.  Create HTML/JavaScript Mobile Service with (free) 20MB SQL store
4.  Download sample todo-list project
5.  Test project and view data store on Azure portal
    
6.  Create new "Products" table with { name, price, count } columns
7.  Update sample project to create/update Products table
8.  Test changes and view data store on Azure portal
    
9.  Create (free) Website Service and set up Git deployment
10. Update cross-origin resource sharing setting for Mobile Service
11. Deploy product listing website