$(function() {
  var client = new WindowsAzure.MobileServiceClient('https://michaelee.azure-mobile.net/', 'CSecLAHuyHPGStjIRTkgDXsnRYOjZk85'),
      productsTable = client.getTable('products');

  function refreshProducts () {
    var query = productsTable.where(function () {
      return this.count > 0;
    });

    query.read().then(function(products) {
      var listItems = $.map(products, function(item) {
        return $('<li>')
          .attr('data-product-id', item.id)
          .attr('data-product-count', item.count)
          .append($('<div>').append(item.name + ' @ $' + item.price + ', left: ' + item.count))
          .append($('<button class="product-order">Order</button>'));
      });

      $('#products').empty().append(listItems).toggle(listItems.length > 0);
    });
  }

  function getProductId(formElement) {
    return $(formElement).closest('li').attr('data-product-id');
  }

  function getProductCount(formElement) {
    return parseInt($(formElement).closest('li').attr('data-product-count'));
  }

  // Handle insert
  $('#add-product').submit(function(evt) {
    var textbox = $('#new-product-name'),
        productName = textbox.val();
    if (productName !== '') {
      productsTable.insert({ name: productName, price: 5.99, count: 10 }).then(refreshProducts);
    }
    textbox.val('').focus();
    evt.preventDefault();
  });

  $(document.body).on('click', '.product-order', function () {
    productsTable.update({ id: getProductId(this), count: getProductCount(this)-1 })
      .then(refreshProducts);
  });

  // On initial load, start by fetching the current data
  refreshProducts();
});
